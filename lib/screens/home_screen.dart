import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp/data/dummy_data.dart';
import 'package:whatsapp/model/chat_model.dart';
import 'package:whatsapp/model/strory_model.dart';
import 'package:whatsapp/widgets/add_story_button.dart';
import 'package:whatsapp/widgets/chat_list_widget.dart';
import 'package:whatsapp/widgets/highlighted_story_widget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<ChatModel> chats = DummyData.chats;
    final List<StoryModel> stories = DummyData.stories;
    double widthSize = MediaQuery.of(context).size.width;
    return Scaffold(
        body: Container(
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.topRight,
                  colors: [
                Color.fromRGBO(24, 74, 36, 1),
                Color.fromRGBO(15, 61, 18, 1),
                Color.fromRGBO(24, 74, 36, 1),
                Color.fromRGBO(5, 10, 6, 1),
              ])),
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.only(top: 45, bottom: 20),
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.only(left: 25, right: 25),
                      margin: const EdgeInsets.only(bottom: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Image.asset('assets/img/whatsapp.png',
                                  width: 35, height: 35),
                              Container(
                                margin: const EdgeInsets.only(left: 15),
                              ),
                              const Text("What's App",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20))
                            ],
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: const Icon(Icons.search),
                            iconSize: 35,
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 80,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        physics: const BouncingScrollPhysics(),
                        children: [
                          const AddStoryButton(),
                          ...List.generate(stories.length, (index) {
                            return HighlightedStoryWidget(story: stories[index]);
                          }).toList()
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Flexible(
                child: Container(
                  padding: const EdgeInsets.only(top: 20, left: 25, right: 25),
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      )),
                  child: Column(
                    children: [
                      Container(
                        height: 45,
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.grey.withOpacity(0.1)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: widthSize * 0.4,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.white),
                              child: TextButton(
                                  onPressed: () {},
                                  child: Text('Chats',
                                      style: TextStyle(color: Colors.black.withOpacity(0.7)))),
                            ),
                            SizedBox(
                              width: widthSize * 0.4,
                              child: TextButton(
                                  onPressed: () {},
                                  child: const Text('Group',
                                      style: TextStyle(color: Colors.grey))),
                            ),
                          ],
                        ),
                      ),
                      Flexible(child: ListView(
                        padding: const EdgeInsets.all(0),
                        physics: const BouncingScrollPhysics(),
                        children: List.generate(chats.length, (index) {
                          return Container(
                            margin: const EdgeInsets.only(top: 15, bottom: 15),
                            child: ChatListWidget(chat: chats[index],),
                          );
                        }),
                      ))
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        bottomNavigationBar: Container(
          height: 100,
          decoration: BoxDecoration(
            border: Border(top: BorderSide(width: 1, color: Colors.grey.withOpacity(0.1)))
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                enableFeedback: false,
                onPressed: () {},
                icon: Icon(
                  Icons.home_rounded,
                  color: Colors.green.withOpacity(0.8),
                  size: 30,
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.call_outlined,
                  color: Colors.grey,
                  size: 30,
                ),
              ),
              Container(
                height: 55,
                width: 55,
                decoration: BoxDecoration(
                    gradient: const LinearGradient(colors: [
                      Colors.blueAccent,
                      Colors.green,
                    ]),
                    borderRadius: BorderRadius.circular(50)),
                child: IconButton(
                  enableFeedback: false,
                  onPressed: () {},
                  icon: const Icon(
                    Icons.add,
                    color: Colors.white,
                    size: 30,
                  ),
                ),
              ),
              IconButton(
                enableFeedback: false,
                onPressed: () {},
                icon: const Icon(
                  Icons.camera_alt_outlined,
                  color: Colors.grey,
                  size: 30,
                ),
              ),
              IconButton(
                enableFeedback: false,
                onPressed: () {},
                icon: const Icon(
                  Icons.person_outline_rounded,
                  color: Colors.grey,
                  size: 30,
                ),
              ),
            ],
          ),
        ));
  }
}
