import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:whatsapp/screens/home_screen.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.DASHBOARD,
        page: () => const HomeScreen(),
        transition: Transition.rightToLeft)
  ];
}
