import 'package:whatsapp/model/chat_model.dart';
import 'package:whatsapp/model/strory_model.dart';

class DummyData {
  static List<ChatModel> chats = [
    ChatModel('Tony', 'people1.jpg', 'Hi Mirlan, how u doing ?', true, 2, '08:11', true, 1),
    ChatModel('Jake', 'people2.jpg', 'LOL I forgot it', false, 2, '09:11', false, 1),
    ChatModel('Lisa', 'people5.jpg', 'Hi Mirlan, Can we talk ?', true, 1, '10:01', false, 1),
    ChatModel('Alex', 'people4.jpg', '01:02', true, 2, '11:11', true, 2),
    ChatModel('Sarah', 'people3.jpg', 'Well, both are good!', false, 0, '12:11', true, 1),
    ChatModel('Qubix', 'people6.jpg', 'No worries', true, 2, '14:11', true, 1),
    ChatModel('Mark', 'people7.jpg', 'All good, how u doing ?', true, 2, '14:21', true, 1),
    ChatModel('Anna', 'people8.jpg', '00:30', false, 2, '16:11', true, 2),
    ChatModel('Aldi', 'people9.jpg', 'Nice catch mate!', true, 4, '19:11', false, 1),
    ChatModel('Mike', 'people10.jpg', 'All good', true, 1, '20:11', true, 1),
  ];

  static List<StoryModel> stories = [
    StoryModel('Tony', 'people1.jpg'),
    StoryModel('Alex', 'people4.jpg'),
    StoryModel('Sarah', 'people3.jpg'),
    StoryModel('Anna', 'people8.jpg'),
    StoryModel('Mike', 'people10.jpg'),
    StoryModel('Mark', 'people7.jpg'),
  ];
}