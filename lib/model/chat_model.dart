class ChatModel {
  final String peopleName;
  final String peoplePicture;
  final String lastMessages;
  final bool isMessageUnread;
  final int unreadMessageCount;
  final String timeMessageDelivered;
  final bool isHasActiveStatus;
  final int chatType;

  ChatModel(
      this.peopleName,
      this.peoplePicture,
      this.lastMessages,
      this.isMessageUnread,
      this.unreadMessageCount,
      this.timeMessageDelivered,
      this.isHasActiveStatus,
      this.chatType);
}
