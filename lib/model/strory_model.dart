class StoryModel {
  final String name;
  final String profile;

  StoryModel(this.name, this.profile);
}