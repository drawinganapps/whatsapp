import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddStoryButton extends StatelessWidget {
  const AddStoryButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 25),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          DottedBorder(
            dashPattern: const [6],
            borderType: BorderType.RRect,
            radius: const Radius.circular(50),
            color: Colors.grey,
            padding: const EdgeInsets.all(1),
            strokeWidth: 2,
            child: ClipRRect(
              child: Container(
                padding: const EdgeInsets.all(4),
                child: IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.add, color: Colors.white)),
              ),
            ),
          ),
          const Text('Add',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                  fontWeight: FontWeight.w300))
        ],
      ),
    );
  }
}
