import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp/model/chat_model.dart';

class ChatListWidget extends StatelessWidget {
  final ChatModel chat;

  const ChatListWidget({Key? key, required this.chat}) : super(key: key);

  Widget userProfileWidgetWithActiveStatus() {
    return Container(
      height: 65,
      padding: const EdgeInsets.all(3),
      margin: const EdgeInsets.only(right: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        gradient: const LinearGradient(
            colors: [Colors.green, Colors.blueAccent],
            transform: GradientRotation(90)),
      ),
      child: Container(
        padding: const EdgeInsets.all(3),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50), color: Colors.white),
        child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
            ),
            clipBehavior: Clip.antiAlias,
            child: Image.asset(
              'assets/img/profiles/${chat.peoplePicture}',
              width: 50,
              height: 50,
              fit: BoxFit.cover,
            )),
      ),
    );
  }

  Widget userProfileWidgetWithoutActiveStatus() {
    return Container(
      height: 65,
      padding: const EdgeInsets.all(3),
      margin: const EdgeInsets.only(right: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
      ),
      child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
          ),
          clipBehavior: Clip.antiAlias,
          child: Image.asset(
            'assets/img/profiles/${chat.peoplePicture}',
            width: 60,
            height: 60,
            fit: BoxFit.cover,
          )),
    );
  }

  Widget voiceChat() {
    return Row(
      children: [
        const Icon(Icons.mic_outlined, color: Colors.blue, size: 20),
        Text(chat.lastMessages,
            style: TextStyle(
                fontSize: 15,
                color: chat.isMessageUnread
                    ? Colors.black.withOpacity(0.8)
                    : Colors.grey))
      ],
    );
  }
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            chat.isHasActiveStatus
                ? userProfileWidgetWithActiveStatus()
                : userProfileWidgetWithoutActiveStatus(),
            SizedBox(
              height: 45,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(chat.peopleName,
                      style: const TextStyle(
                          fontWeight: FontWeight.w700, fontSize: 16)),
                  chat.chatType == 1 ? Text(chat.lastMessages,
                      style: TextStyle(
                          fontSize: 15,
                          color: chat.isMessageUnread
                              ? Colors.black.withOpacity(0.8)
                              : Colors.grey)) : voiceChat()
                ],
              ),
            )
          ],
        ),
        Column(
          children: [
            Text(chat.timeMessageDelivered,
                style: TextStyle(color: Colors.black.withOpacity(0.8))),
            chat.isMessageUnread
                ? Badge(
                    badgeColor: Colors.pinkAccent.withOpacity(0.9),
                    badgeContent: Container(
                      padding: const EdgeInsets.all(4),
                      child: Text(chat.unreadMessageCount.toString(),
                          style: const TextStyle(
                              color: Colors.white,
                              fontSize: 10,
                              fontWeight: FontWeight.bold)),
                    ),
                  )
                : Container()
          ],
        )
      ],
    );
  }
}
